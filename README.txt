=============================
Pacman Projekt v Jave s Maven
=============================

Prehľad:
--------
Tento projekt Pacman je rekreácia klasickej arkádovej hry, kde hráči ovládajú Pacmana, ktorý sa pohybuje v bludisku. Cieľom je zozbierať všetky kľúče a nájsť konečnú bránu. 
Hra končí vítaztvom, ak sa vám podarí pozbierať všetky kľúče a nájsť bránu alebo prehrou, ak PacMan stratí všetky životy.

Technológie:
------------
- Java
- Maven

Autori:
-------
- Tomáš Bražina (xbrazi01)
- Matěj Vrba, (xvrbam03)

Poznámky:
---------
- Hra bola vytvorená pomocou Javy a Maven pre zjednodušenie závislostí a zostavenia projektu.

Spustenie hry:
--------------
1. Sklonujte alebo stiahnite repozitár.
2. Prejdite do adresára projektu a spustite príkaz `mvn` pre spustenie programu.
3. Príkaz `mvn javadoc:javadoc` slúži na vytvorenie dokumentácie.
4. Pri spúšťaní cez príkazový riadok je možné zvoliť mapu pomocou prepínača `--map` alebo nahrať replay pomocou `--replay`

Hra:
----
Pri spustení hry sa najskôr otvorí súborový prehliadač, kde bude potrebné zvoliť .txt súbor s mapou. Taktiež môžete len zatvoriť tento prehliadač a hra sa načíta so predvolenou mapou.

Priklad ako môže mapa vyzerať:
10 10 -- reprezentuje počet riadkov a stĺpcov
.........T
XX...XX...
X.........
XXXXXXX...
....XG....
XX..XGX...
....XXX...
XX...XX.K.
S.........
XX...XX...

Legenda pre mapu:
T - cieľové políčko hry (Target)
X - stena (Wall)
G - duch (Ghost) - začínajúca pozícia ducha
K - kľúč (Key)
. - voľné políčko (Free)
S - štart (Start) - začínajúca pozícia PacMana

Ovládanie hry:
--------------
- Šípky na klávesnici alebo pomocou WASD - pohyb PacMana
- Pomocou klávesy End sa prehrávanie uloží
- medzera - zastavenie prehrávania
- I - krokovanie
- R - spätné prehrávanie
- K/L - Prehranie vpred
- H/J - Prehranie späť
