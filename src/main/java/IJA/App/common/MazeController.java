/**
 * Class controlling maze using Commands
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;

import IJA.App.tool.common.CommonMaze;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.TreeMap;
import java.io.Serializable;


/**
 * Class controlling maze using commands.
 *
 * 2 important concepts:
 * - command stack - all new commands are pushed onto stack(to save memory
 * implemented using tree map). Stack contains array of commands per one game
 * frame.
 * - head - "pointer" to the stack, during gameplay points at the top of the
 * stack, during replay moves from start to the end, pointing at commands
 * currrently being replayed.
 *  */
public class MazeController implements Serializable{
	//list of commands, (Frame number to array of commands map)
	TreeMap<Integer, ArrayList<Command>> commands;
	// Current frame number
	int head;
	static final long serialVersionUID = 42L;

	public MazeController(CommonMaze maze){
		commands = new TreeMap<Integer, ArrayList<Command>>();
		head = 0;
	}

	/**
	 * Executes and saves command
	 *  */
	public void exec(Command cmd, CommonMaze maze){
		if(commands.get(head) == null){
			commands.put(head, new ArrayList<Command>());
		}
		commands.get(head).add(cmd);
		cmd.exec(maze);
	}

	/**
	 * Ends one frame and starts another
	 *  */
	public void step(){
		head += 1;
	}

	/**
	 * Undoes all commands in current frame and moves head one frame back
	 *
	 * @return number of undone commands, -1 if head is at beginning of history
	 *  */
	public int undo(CommonMaze maze){
		// list of commands from this step
		ArrayList<Command> cmds;
		int ret = 0;
		if(head >= 0){
			ret = 0;
			cmds = commands.get(head);
			head -= 1;
		}else{
			return -1;
		}

		if(cmds != null){

			// iterator over current commands
			var cmdi = cmds.listIterator(cmds.size());

			// iterating in reverse
			while(cmdi.hasPrevious()){
				var cmd = cmdi.previous();
				cmd.undo(maze);
				ret += 1;
			}
		}
		return ret;
	}

	/**
	 * oposite of `undo`. reexecutes all commands in current frame and moves head
	 * forward.
	 *  */
	public int replay(CommonMaze maze){
		// list of commands from this step
		ArrayList<Command> cmds;
		int ret = 0;
		if(headBehind()){
			ret = 0;
			cmds = commands.get(head);
			head++;
		}else{
			return -1;
		}

		if(cmds != null){
			// iterator over current commands
			var cmdi = cmds.listIterator(cmds.size());

			// iterating in reverse
			while(cmdi.hasPrevious()){
				var cmd = cmdi.previous();
				cmd.exec(maze);
				ret += 1;
			}
		}
		return ret;
	}

	/**
	 * Prints all non-empty fram numbers and their commands
	 *  */
	public void print(){
		for(var entry : commands.entrySet()){
			System.err.printf("frame %d:%n", entry.getKey());
			for(Command v: entry.getValue())
				System.err.println(v);
		}

	}

	/**
	 * returns whether head is pointing at the top of the stack
	 *
	 * @return true if head is not pointing at the top, false otherwise
	 *  */
	public boolean headBehind(){
		int largest = 0;
		for(var k : commands.keySet()){
			if (k > largest)
				largest = k;
		}
		return head < largest;
	}

	/**
	 * returns whether head is pointing at the beginning of the stack
	 *
	 * @return true if head is at beginning
	 *  */
	public boolean headAtStart(){
		return head <= 0;
	}

	/**
	 * after loading from saved replay, this function should be called to avoid
	 * unexpected behaviour.
	 *  */
	public void resumeAfterLoad(){
		// when game is saved, head is serialized and saved as well, but after
		// loading replay, map is loaded in initial state(as if game just started)
		// this would cause the game in replay mode to execute commands from the end
		// of the replay on objects that are in initial position and state
		this.head = 0;
	}

    /**
     * Uspani vlakna na zadany pocet ms.
     * @param ms Pocet ms pro uspani vlakna.
     */
    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Logger.getLogger(MazeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
