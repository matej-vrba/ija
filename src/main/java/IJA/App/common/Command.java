/**
 * Implementation of Command design pattern
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;
import IJA.App.tool.common.CommonMaze;
import java.io.*;


/**
 * Class implementing command design pattern.
 *
 * Other classes should derive from this one in order to implement specific commands
 *
 *  */
public class Command<T> implements Serializable{
		/** Subject of command */
	String PID;

		public interface func<T> {
				void run(T obj);
		}

		/**
		 *  Constructor of command
		 *
		 *  @param PID Persistent ID of object to execute this command on
		 *  */
		public Command(String PID){
				this.PID = PID;
		}

		/**
		 * Performs change made by this command
		 *  */
		public void exec(CommonMaze maze){
			System.err.println("Not implemented");
			System.exit(1);
		}

		/**
		 * Undos change made by this command
		 *  */
		 public void undo(CommonMaze maze){
			System.err.println("Not implemented");
			System.exit(1);

		}
	public String toString(){
		return String.format("Generic cmd on %s", PID);
	}
}
