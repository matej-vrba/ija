/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;

import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMazeObject;

public interface Field extends CommonField {

    /**
     * Determines if an object can be placed on the field.
     * @return true if the object can be placed, false otherwise
     */
    boolean canMove();

    /**
     * Returns the object currently located on the field.
     * @return the object on the field, or null if the field is empty
     */
    CommonMazeObject get();

    /**
     * Determines if the field is empty.
     * @return true if the field is empty, false otherwise
     */
    boolean isEmpty();

    /**
     * Returns the neighboring field in the specified direction.
     * @param dirs the direction to look for the neighboring field
     * @return the neighboring field in the specified direction, or null if there is no field in that direction
     */
    CommonField nextField(CommonField.Direction dirs);

    /**
     * Places a maze object on the field.
     * @param object the maze object to place on the field
     * @return true if the object was successfully placed, false otherwise
     */
    boolean put(CommonMazeObject object);

    /**
     * Removes the specified object from the field.
     * @param object the object to remove from the field
     * @return true if the object was successfully removed, false otherwise
     */
    boolean remove(CommonMazeObject object);

    /**
     * Moves an object from the current field to the specified destination field.
     * @param destination the field to move the object to
     * @return true if the object was successfully moved, false otherwise
     */
    boolean move(CommonField destination);

}