/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;

public class NotYetImplemented extends RuntimeException {
    public NotYetImplemented() {
        super("Not Yet Implemented");
    }
}
