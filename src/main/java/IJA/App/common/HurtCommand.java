/**
 * Implementation of command to change ammout of pacman's lives
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.game.PacmanObject;
import java.io.Serializable;

/**
 * Damages pacman by removing 1 hp, undoing readds it
 *  */
public class HurtCommand extends Command<CommonMazeObject> implements Serializable{
	/**
	 * @param PID persistent ID of pacman
	 *  */
		public HurtCommand( String PID){
				super(PID);
		}

		@Override
		public void exec(CommonMaze maze){
				var pac = (PacmanObject)maze.getByPID(PID);//TODO null
				pac.setLives(pac.getLives() - 1);
		}

		@Override
		public void undo(CommonMaze maze){
				var pac = (PacmanObject)maze.getByPID(PID);//TODO null
				pac.setLives(pac.getLives() + 1);
		}

		public String toString(){
				return String.format("change lives %s", PID);
		}
}
