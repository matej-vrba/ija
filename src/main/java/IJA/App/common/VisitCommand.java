/**
 * Implementation of command to set field to visited state
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonMazeObject;
import java.io.Serializable;


public class VisitCommand extends Command<CommonMazeObject> implements Serializable{
		private int x, y;
	/**
	 * Expects x and y position of field that was visited
	 *
	 *  */
		public VisitCommand(int x, int y){
				super(null);
				this.x = x;
				this.y = y;
		}

		@Override
		public void exec(CommonMaze maze){
				maze.getField(x,y) .setVisited(true);
		}

		@Override
		public void undo(CommonMaze maze){
				maze.getField(x,y) .setVisited(false);
		}

		public String toString(){
				return String.format("visit %d %d", x,y);
		}
}
