/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;

import IJA.App.game.GhostObject;
import IJA.App.game.PacmanObject;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonField;
import java.io.ObjectOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import java.util.HashMap;

/**
 * Represents a maze containing fields, ghosts, and a Pac-Man character. This class
 * provides functionality for managing the state and interactions of these elements
 * within the maze, as well as utility methods for working with the maze dimensions.
 */
public class Maze extends JPanel implements CommonMaze {
	CommonField[][] fields;
	List<GhostObject> ghosts;
	PacmanObject PacMan;
	int mapHeight, mapWidth;
	int numKeys;
	// NOTE: might be faster using 2d array
	HashMap<String, CommonMazeObject> Objects = new HashMap<String, CommonMazeObject>();
	boolean showMarks = false;
	List<String> source;

	 /**
     * Constructs a new Maze instance with the specified fields, ghosts, Pac-Man character, and keys.
     * @param fields the 2D array of CommonField objects representing the maze's layout
     * @param ghosts the list of ghosts within the maze
     * @param PacMan the Pac-Man character within the maze
     * @param keys the number of keys in the maze
     */
	public Maze(CommonField[][] fields, List<GhostObject> ghosts, PacmanObject PacMan, int keys) {
		this.fields = fields;
		this.PacMan = PacMan;
		this.numKeys = keys;

		if(ghosts != null){
			this.ghosts = ghosts;
		}else{
			// In case Maze is initalized with no ghosts it can be expected that ghosts
			// will be added later -> mutable list
			this.ghosts = new ArrayList<GhostObject>();
		}
	}

	
	public CommonField getField(int row, int col) {
		return fields[row][col];
	}

	
	public int numCols() {
		return fields[0].length;
	}

	
	public int numRows() {
		return fields.length;
	}

	public CommonMazeObject getByPID(String PID){
		return Objects.get(PID);
	}

	public void index(){
		for(var row : fields){
			for(var cell: row){
				if(!cell.isEmpty()){
					Objects.put(cell.get().getPID(), cell.get());
				}
			}
		}
		for(var ghost: ghosts){
			Objects.put(ghost.getPID(), ghost);
		}
		Objects.put(PacMan.getPID(), PacMan);

	}

	public void add_ghost(GhostObject ghost){
		this.ghosts.add(ghost);
	}

	@Override
	public List<GhostObject> ghosts() {
		return new ArrayList<GhostObject>(this.ghosts);
	}

	public PacmanObject getPacManObject(){
		return this.PacMan;
	}

	public void setHeightWidth(int height, int width){
		this.mapHeight = height;
		this.mapWidth = width;
	}

	public int getHeight(){
		return this.mapHeight;
	}

	public int getWidth(){
		return this.mapWidth;
	}

	public int getKeys(){
		return this.numKeys;
	}

	public void setKey(int num){
		this.numKeys = num;
	}
	
	public boolean showMarks(){
		return showMarks;
	}

	public void toggleMarks(){
		showMarks = !showMarks;
	}

	public void serialize(ObjectOutputStream out)throws IOException{
		
		out.writeInt(this.source.get(0).length());
		out.writeInt(this.source.size());
		for(String line: this.source){
			out.writeObject(line);
		}
	}

	public void setSource(List<String> src){
		this.source = src;
	}

	public int clampX(int input){
		return Math.max(0, Math.min(input, this.getWidth()));
	}

	public int clampY(int input){
		return Math.max(0, Math.min(input, this.getHeight()));
	}
}
