/**
 * Implementation of command to disable object
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonMazeObject;
import java.io.Serializable;


/**
 * executing command disables the object, undoing re-enables it
 *
 *  */
public class KeyPickupCommand extends Command<CommonMazeObject> implements Serializable{
	/**
	 * @param PID persistent ID of object to disable
	 *  */
		public KeyPickupCommand(String PID){
				super(PID);
		}

		@Override
		public void exec(CommonMaze maze){
				maze.getByPID(PID) .setDisabled(true);
				maze.setKey(maze.getKeys() - 1);
		}

		@Override
		public void undo(CommonMaze maze){
				maze.getByPID(PID) .setDisabled(false);
				maze.setKey(maze.getKeys() + 1);
		}

		public String toString(){
				return String.format("Disable %s", PID);
		}
}
