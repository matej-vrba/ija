/**
 * Implementation of command to change pacman's direction
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.common;
import IJA.App.tool.common.CommonField.Direction;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonMaze;
import java.io.*;


/**
 * Command to change movement direction of CommonMovable
 *  */
public class ChangeDirCommand extends Command<CommonMazeObject> implements Serializable{
	private Direction new_dir;
	private Direction old_dir;


	/**
	 * Saves old direction of object identified by PID
	 *
	 * @param maze initialize CommonMaze containing object identified by PID
	 * @param PID persistent ID of target
	 * @param new_dir new direction of PID
	 *  */
		public ChangeDirCommand(CommonMaze maze, String PID, Direction new_dir){
				// Initialize base class with null because base constructor cannot refer to `this`
				super(PID);
				this.PID = PID;
				var object = maze.getByPID(PID);
				// save directions
				this.old_dir = object.getDirection();
				this.new_dir = new_dir;
		}

	@Override
	public void exec(CommonMaze maze){
		var obj = maze.getByPID(PID);//TODO null
					if(new_dir == obj.getDirection())
						return;
					this.old_dir = obj.getDirection();
					obj.setDirection(this.new_dir);
	}

	@Override
	public void undo(CommonMaze maze){
		maze.getByPID(PID)
					.setDirection(this.old_dir);
	}
	public String toString(){
		return String.format("CDC %s %s -> %s", PID, old_dir, new_dir);
	}
}
