/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App;

import IJA.App.game.MazeConfigure;
import java.io.IOException;
import java.io.FileNotFoundException;

import IJA.App.tool.MazePresenter;
import IJA.App.tool.common.CommonMaze;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * App class is the main entry point of the application.
 */
public class App {

     /**
     * Creates and displays the GUI for the maze game.
     * @param args command line arguments passed to the application, used for configuring the maze
     */
    private static void createAndShowGUI(String[] args) {
		MazeConfigure cfg = null;
		try {
			cfg = new MazeConfigure(args);
		} catch (FileNotFoundException e) {
			System.err.println("Failed to open file: " + e.getMessage());
			System.exit(1);
		} catch(IOException e){
			System.err.println("Failed to read file: " + e.getMessage());
			System.exit(1);
		}catch (Exception e){
			System.err.println("Failed to parse data: " + e.getMessage());
			System.exit(1);
		}

        CommonMaze maze = cfg.createMaze();
        
        MazePresenter presenter = new MazePresenter(maze, cfg.isReplay(), cfg.getController());
        presenter.open();
    }

    public static void main(String[] args) {
        createAndShowGUI(args);
    }
    /**
     * Sleep the thread for the specified duration in ms.
     * @param ms Number of milisecodns for thread sleep.
     */
    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
