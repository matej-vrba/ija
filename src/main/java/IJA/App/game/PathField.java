/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;

import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.Observer;
import IJA.App.common.VisitCommand;
import IJA.App.common.MazeController;

import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.awt.Color;


/**
 * The PathField class represents a field in the maze, and is responsible for
 * managing the objects that occupy it, as well as tracking its position in the maze.
 * It implements the CommonField interface.
 */
public class PathField implements CommonField {
	CommonMazeObject object = null;
	
	CommonMaze maze = null;
	int row, col;
	int num_notifications = 0;
	byte neighbor_val = 0;
	boolean visited = false;

	List<Observer> observers = new ArrayList<>();

	public int cost;
	public PathField prev;

	/**
     * Constructs a PathField with the specified row and column.
     * @param row the row index of the PathField in the maze
     * @param col the column index of the PathField in the maze
     */
	public PathField(int row, int col) {
		this.row = row;
		this.col = col;		
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof PathField)) {
			return false;
		}
		if (this == obj) {
			return true;
		}

		PathField other = (PathField) obj;
		return other.row == this.row && other.col == this.col;
	}

	public boolean isEmpty() {
		if (this.object == null){
			return true;
		}
		else {
			return false;
		}
	}

	// sets `object` as object standing on this field
	// true is returned on suecess (allways)
	public boolean put(CommonMazeObject object) {
		this.object = object;
		return true;
	}

	
	/**
     * Removes the object from this field.
     * @return true if the object was removed or the field was already empty, false otherwise
     */
	public boolean remove() {

		if (this.object != null){
			this.object = null;
			return true;
		}
		else if (this.object == null){
			return true;
		}
		return false;
	}

	@Override
	public void setMaze(CommonMaze maze) {
		this.maze = maze;
	}

	/**
     * @return next field in dirrection `dir`, if next field is on the edge of the
	 * maze (in 3x3 maze it would be x or y 0 or 3) it returns new instance of wall
	 * field - this is to include implicit wall arround the maze
     */
	public CommonField nextField(Direction dir) {
		switch (dir) {
		case U:
			return maze.getField(row - 1, col);
		case D:
			return maze.getField(row + 1, col);
		case L:
			return maze.getField(row, col - 1);
		case R:
			return maze.getField(row, col + 1);
		case STOP:
			return this;
		}
		
		return null;
	}

	/**
     * @return object "standing" on this field
     */
	@Override
	public CommonMazeObject get() {
		return this.object;
	}

	/**
     * @return true if object obj is "standing" on current(this) field
     */
	@Override
	public boolean contains(CommonMazeObject obj) {
		if (this.object == obj){
			return true;
		}
		else {
			return false;
		}
	}

	/**
     * Notifies all previously added observers about a change in this field.
     */
	@Override
	public void notifyObservers(){
		for(Observer o: observers){
				this.num_notifications++;
				o.update(this);
			}
	}

	@Override
	public void removeObserver(Observer o){
		observers.remove(o);
	}

	@Override
	public void addObserver(Observer o){
		observers.add(o);
	}

	public void clear_counter_notification(){
		num_notifications = 0;
	}

	public int num_of_notifications(){
		return num_notifications;
	}

	public int get_x(){
		return row;
	}

	public int get_y(){
		return col;
	}

	public CommonMaze get_maze(){
		return this.maze;
	}

	public void setNeighborVal(byte neighbors){
		neighbor_val = neighbors;
	}
	public byte getNeighborVal(){
		return neighbor_val;
	}

	public void visit(boolean visited, MazeController controller){
		if(visited())return;
		var cmd = new VisitCommand(row,col);
		controller.exec(cmd, maze);
	}
	public void setVisited(boolean visited){
		this.visited = visited;
	}
	public boolean visited(){
		return visited;
	}

	TreeMap<Integer, Color> marks = new TreeMap<Integer, Color>();
	public void debugMark(Color c, int markNum){
		marks.put(markNum, c);
	}
	public void clearMark(int markNum){
		marks.put(markNum, null);
	}
	public TreeMap<Integer, Color> getMarks(){
		return marks;
	}
	public boolean isMarked(){
		for(var mark : marks.values()){
			if(mark != null)
				return true;
		}
		return false;
	}
}
