/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;

import IJA.App.tool.common.CommonField.Direction;
import IJA.App.common.NotYetImplemented;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.view.ComponentView;
import IJA.App.common.KeyPickupCommand;
import IJA.App.common.MazeController;
import java.awt.Image;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import java.util.List;
import java.util.ArrayList;

/**
 * This class represents a KeyObject, which is a maze object that can be picked up by the pacman.
 * The class implements the CommonMazeObject interface and provides methods for managing its position,
 * appearance, and interaction with other objects.
 */
public class KeyObject implements CommonMazeObject {
	CommonField parent = null;
	int x, y;
	boolean disabled = false;
	MazeController controller;
	private List<Image> images;
	String PID;

	/**
     * Constructs a new KeyObject with a specified parent field.
     * The object will be placed on the same position as the parent field.
     * @param parent the parent CommonField
     */
	public KeyObject(CommonField parent) {
		this.parent = parent;
		this.x = parent.get_x();
		this.y = parent.get_y();
        this.images = new ArrayList<Image>();

		try {
            File file = new File("lib/key.png");
            images.add(ImageIO.read(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public boolean canMove(CommonField.Direction dir) {
		throw new NotYetImplemented();
	}

	public boolean move() {
		throw new NotYetImplemented();
	}

    public void updatePosition(){
		throw new NotYetImplemented();
	}
	public void setController(MazeController cnt){
		this.controller = cnt;
	}

	public CommonField getField() {
		throw new NotYetImplemented();
	}


    public ComponentView getObjView() {
		throw new NotYetImplemented();
	}
	
	public void setObjView(ComponentView obj_view) {
		throw new NotYetImplemented();
	}

    public void setXY(int x, int y){
		throw new NotYetImplemented();
	}

	public int get_x(){
		return this.x;
	}

	public int get_y(){
		return this.y;
	}

	public List<Image> getImage(){
		return images;
	}

	public void setImage(Image image){
		this.images.add(image);
	}

	/**
     * Returns whether the KeyObject is disabled or not.
     * @return true if disabled, false otherwise
     */
	public boolean isDisabled(){
		return disabled;
	}

	public void disable(){
		var cmd = new KeyPickupCommand(this.getPID());
		controller.exec(cmd, this.parent.get_maze());
	}

	@Override
	public void setDisabled(boolean dis){
		disabled = dis;
	}

	public Direction getDirection(){
		throw new UnsupportedOperationException("Cannot get direction of key");
	}

	public void setDirection(Direction d){
		throw new UnsupportedOperationException("Cannot set direction of key");
	}
	public String getPID(){
		return this.PID;
	}
	public void setPID(String PID){
		this.PID = PID;
	}
}
