/**
 * Implementation of common methods for all moving objects.
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;


import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonField.Direction;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.view.ComponentView;
import IJA.App.common.MazeController;
import java.awt.Image;
import java.awt.Color;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Base class for movable objects
 *
 *
 * variable `speed_multiplier` should be set in derived class constructor,
 * defaults to 1.0 (speed of pacman)
 *
 */
public abstract class CommonMovable implements CommonMazeObject {
	protected CommonField parent = null;
	// current row and column, calculated from `x` and `y`
	protected int row, col;
	protected int x, y;
	// dimensions ob the object, used to calculate `row` and `col`
	protected int objectHeight, objectWidth;
	// speed calculated based on `speed_multiplier`
	protected int speed;
	protected List<Image> images;
	protected CommonField.Direction direction;
	protected CommonField.Direction nextDirection;
	protected ComponentView obj_view = null;
	// Maze Controller, crucial for undo and replay
	public MazeController controller;
	protected float speed_multiplier = 1.0f;
	protected String PID;

	public CommonMovable(CommonField parent) {
		this.parent = parent;
		this.row = parent.get_x();
		this.col = parent.get_y();
		this.images = new ArrayList<Image>();
		this.direction = CommonField.Direction.STOP;
		this.nextDirection = CommonField.Direction.STOP;
		updateSpeed();
	}

	/**
	 * Moves object +`speed` in `direction`, if `direction` is
	 * Direction.STOP does nothing
	 *
	 * @return true if object was moved, false otherwise
	 */
	public boolean move() {
		if (this.direction == Direction.D)
			moveDown();
		else if (this.direction == Direction.U)
			moveUp();
		else if (this.direction == Direction.L)
			moveLeft();
		else if (this.direction == Direction.R)
			moveRight();
		else
			return false;

		return true;
	}

	public void moveUp() {
		int nextPosition = this.y - this.speed;
		this.y = nextPosition;
		onMoveUp();
	}

	public void moveDown() {
		int nextPosition = this.y + this.speed;
		this.y = nextPosition;
		onMoveDown();
	}

	public void moveLeft() {
		int nextPosition = this.x - this.speed;
		this.x = nextPosition;
		onMoveLeft();
	}

	public void moveRight() {
		int nextPosition = this.x + this.speed;
		this.x = nextPosition;
		onMoveRight();
	}

	void updateSpeed() {
		this.speed = (int) (4.0 * speed_multiplier);
	}

	public void reverseSpeed() {
		speed = -speed;
	}

	/** Called every frame after moving object up */
	public void onMoveUp() {
	}

	/** Called every frame after moving object down */
	public void onMoveDown() {
	}

	/** Called every frame after moving object left */
	public void onMoveLeft() {
	}

	/** Called every frame after moving object right */
	public void onMoveRight() {
	}

	/**
	 * returns true if object can move in direction `dir`
	 *
	 * This check is based on if next field is Path (not if it's empty)
	 */
	public boolean canMove(CommonField.Direction dir) {
		return parent.nextField(dir) instanceof PathField;
	}

	public void setController(MazeController cnt) {
		this.controller = cnt;
	}

	public void setObjectHeightWidth(int objectHeight, int objectWidth) {
		setObjectWidthHeight(objectWidth, objectHeight);
	}

	public void setObjectWidthHeight(int objectWidth, int objectHeight) {
		this.objectHeight = objectHeight;
		this.objectWidth = objectWidth;
	}

	public int getLives() {
		throw new UnsupportedOperationException(
				"GetLives is not implemented for ghost object");
	}

	public Direction getDirection() {
		return this.direction;
	}

	public void setDirection(Direction d) {
		this.direction = d;
	}

	public void updatePosition() {
		this.move();
	}

	public void setImage(Image image) {
		this.images.add(image);
	}

	public List<Image> getImage() {
		return this.images;
	}

	public void setNextDir(CommonField.Direction dir) {
		this.nextDirection = dir;
	}

	public CommonField.Direction getNextDir() {
		return this.nextDirection;
	}

	public ComponentView getObjView() {
		return obj_view;
	}

	public void setObjView(ComponentView obj_view) {
		this.obj_view = obj_view;
	}

	public int getSpeed() {
		return this.speed;
	}

	public void setRowCol(int row, int col) {
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return this.row;
	}

	public int getCol() {
		return this.col;
	}

	public int get_x() {
		return this.x;
	}

	public int get_y() {
		return this.y;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public CommonField getField() {
		return this.parent;
	}

	public int getObjectHeight() {
		return this.objectHeight;
	}

	public int getObjectWidth() {
		return this.objectWidth;
	}

	public boolean isPacman() {
		return false;
	}

	public String getPID() {
		return this.PID;
	}

	public void setPID(String PID) {
		this.PID = PID;
	}

	/**
	 * Initializes all fields, called at the beginning of djikstra's algorithm
	 *
	 * initalization consists of setting parent to null and cost to infinity (9999)
	 *
	 * @return list of all fields, to be searched
	 *  */
	HashSet<PathField> djikstraInit() {
		HashSet<PathField> openFields = new HashSet<PathField>();
		for (int i = 0; i < this.parent.get_maze().numRows(); i++) {
			for (int j = 0; j < this.parent.get_maze().numCols(); j++) {
				var p = this.parent.get_maze().getField(i, j);
				if (p instanceof PathField) {
					((PathField) p).cost = 9999;
					((PathField) p).prev = null;
					openFields.add((PathField) p);
				}
			}
		}
		return openFields;
	}

	/**
	 * searches path from this's position to target x and y
	 *
	 * @param target_x x coor of field to find path to
	 * @param target_y y coor of field to find path to
	 * @param markID - id by which to mark resulting path
	 *
	 *  */
	PathField djikstra(int target_x, int target_y, int markID) {
		HashSet<PathField> openFields = djikstraInit();

		int mazeHeight = this.parent.get_maze().getHeight();
		int mazeWidth = this.parent.get_maze().getWidth();
		CommonMaze maze = this.parent.get_maze();

		// clamp target to <0, width/height>
		target_x = maze.clampX(target_x);
		target_y = maze.clampY(target_y);

		// if current field is wall, check surrounding fields
		if (maze.getField(target_x, target_y) instanceof WallField) {
			if (target_x > 0 && maze.getField(target_x - 1, target_y) instanceof PathField) {
				target_x -= 1;
			} else if (target_y > 0 && maze.getField(target_x, target_y - 1) instanceof PathField) {
				target_y -= 1;
			} else if (target_x < mazeHeight && maze.getField(target_x + 1, target_y) instanceof PathField) {
				target_x += 1;
			} else if (target_y < mazeWidth && maze.getField(target_x, target_y + 1) instanceof PathField) {
				target_y += 1;
			}
		}
		// set cost of starting field to 0
		((PathField) maze.getField(row, col)).cost = 0;

		return djikstraPathfind(openFields, target_x, target_y, markID);

	}

	PathField djikstraPathfind(HashSet<PathField> pathOpen, int target_x, int target_y, int markID) {
		while (!pathOpen.isEmpty()) {
			// find field with best cost
			PathField currentlySearched = (PathField) pathOpen.toArray()[0];
			for (var v : pathOpen) {
				if (v.cost < currentlySearched.cost) {
					currentlySearched = v;
				}
			}
			// set cost accordingly in 4 neighboring fields
			djikstraCheckField(currentlySearched, 1, 0);
			djikstraCheckField(currentlySearched, -1, 0);
			djikstraCheckField(currentlySearched, 0, 1);
			djikstraCheckField(currentlySearched, 0, -1);

			// if coords of currentlySearched eual to target, we've found the destination
			if (currentlySearched.get_x() == target_x &&
				currentlySearched.get_y() == target_y) {
				// at this point we've found path to destination and we need to go back
				// to find where to move from starting position

				// just a rename, to make it little bit more readable
				var dest = currentlySearched;
				// if destination is the start, there was no path and so prev is null
				if (dest.prev == null) {
					return null;
				}
				while (dest.prev.prev != null) {
					dest.debugMark(new Color(255, 0, 0, 50), markID);
					dest = dest.prev;
				}
				return dest;
			}

			pathOpen.remove(currentlySearched);
		}
		return null;
	}

	/**
	 * function to check 4 surrounding fields
	 * if the cost of current field is better, cost and parent (`prev`) is set to
	 * cost = current.cost + 1
	 * parent = current
	 */
	void djikstraCheckField(PathField field, int offsetX, int offsetY) {
		CommonMaze maze = this.parent.get_maze();
		int new_x = field.get_x() + offsetX;
		int new_y = field.get_y() + offsetY;

		if (maze.getField(new_x, new_y) instanceof PathField) {
			var nextField = (PathField) maze.getField(new_x, new_y);
			if (nextField.cost > field.cost) {
				nextField.cost = field.cost + 1;
				nextField.prev = field;
			}
		}
	}
}
