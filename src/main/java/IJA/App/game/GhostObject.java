/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;


import IJA.App.tool.common.CommonField.Direction;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMaze;
import IJA.App.common.ChangeDirCommand;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.Random;

/**
 * GhostObject class represents a ghost character in the maze game.
 * The ghost follows different strategies based on its order and interacts with Pac-Man.
 */
public class GhostObject extends CommonMovable {
	private int counter = 0;
	int order;
	public int dirImage;

	 /**
     * Constructs a GhostObject with the specified parent field and order.
     * Initializes the ghost object's position, speed, and image.
     *
     * @param parent the CommonField that the ghost object belongs to
     * @param order  the order number of the ghost
     */
	public GhostObject(CommonField parent, int order) {
		super(parent);
		this.order = order;
		speed_multiplier = 0.75f;
		updateSpeed();
		int sprite_n = 0;
		switch (order){
				case 0:
					sprite_n = 42;
					break;
				case 1:
					sprite_n = 21;
					break;
				case 2:
					sprite_n = 17;
					break;
				case 3:
					sprite_n = 38;
					break;
				case 4:
					sprite_n = 25;
					break;
				default:
					sprite_n = 34;
					break;
			}
		try {
			for(int i = 0; i < 4; i++) {
				File file = new File(String.format("lib/rosekane_%d.png", sprite_n + i));
				images.add(ImageIO.read(file));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Updates the ghost's movement direction based on its current position,
     * target direction, and movement strategy.
     */
	public void updateDirection(){
		int x_remainder = this.x % this.objectWidth;
		int y_remainder = this.y % this.objectHeight;

		if (x_remainder < speed && y_remainder < speed) {
			for(int i = 0; i < this.parent.get_maze().numRows(); i++){
				for(int j = 0; j < this.parent.get_maze().numCols(); j++){
					var p =	this.parent.get_maze().getField(i,j);
					p.clearMark(order);
				}
			}

			this.col = this.x / this.objectWidth;
			this.row = this.y / this.objectHeight;
			this.parent = this.parent.get_maze().getField(this.row, this.col);

			Direction next_dir = Direction.STOP;
			if (this.order % 2 == 0){
				next_dir = this.findDirToPacman();
			}
			if (this.order % 2 == 1){
				next_dir = this.getRandomDir();
			}

				next_dir = this.findDirToPacman();


		// check if next field in direction `dir` is field we can move to (aka
		// pathField)
			if (!canMove(next_dir)){
				next_dir = Direction.STOP;
			}
			if(this.direction == next_dir)
				return;
			this.nextDirection = next_dir;
			
			var cmd = new ChangeDirCommand(this.parent.get_maze(), getPID(), next_dir);
			controller.exec(cmd, this.parent.get_maze());
		}
	}

	/**
     * Updates Pinky's target position based on its current position,
     * target direction, and movement strategy.
     *
     * @return the target CommonField for Pinky
     */
	CommonField updatePinky(){
		CommonMaze maze = this.parent.get_maze();
		PacmanObject pacman = maze.getPacManObject();
		int pacmanR = pacman.getRow();
		int pacmanC = pacman.getCol();
		int targetR = pacman.getRow();
		int targetC = pacman.getCol();
		Direction pacmanDir = pacman.getDirection();
		CommonField dest;
			int diffR = (pacmanR - row);
			int diffC = (pacmanC - col);
			if(Math.abs(diffC) + Math.abs(diffR) < 2){
				targetR = pacmanR;
				targetC = pacmanC;
			}else{

		switch(pacmanDir){
			case D:
				targetR += 4;
				break;
			case U:
				targetR -= 4;
				break;
			case L:
				targetC -= 4;
				break;
			case R:
				targetC += 4;
				break;
		default:
		}
		if(targetR > maze.getHeight())
			targetR = maze.getHeight() - 1;
		if(targetC > maze.getWidth())
			targetC = maze.getWidth() - 1;
		if(targetR < 0)
			targetR = 0;
		if(targetC < 0)
			targetC = 0;
			}

			dest = djikstra(targetR, targetC, order);

			if(dest != null){
				maze.getField(targetR, targetC) .debugMark(new Color(255,0,255,200), order);
			}
			else{
				maze.getField(targetR, targetC) .debugMark(new Color(255,0,255,100), order);
				maze.getField(pacmanR, pacmanC) .debugMark(new Color(255,0,255,100), order);
			}
			return dest;
	}

	/**
     * Updates Inky's target position based on its current position,
     * target direction, and movement strategy.
     *
     * @return the target CommonField for Inky
     */
	CommonField updateInky(){
		CommonMaze maze = this.parent.get_maze();
		PacmanObject pacman = maze.getPacManObject();
		int pacmanR = pacman.getRow();
		int pacmanC = pacman.getCol();
		int targetR = pacman.getRow();
		int targetC = pacman.getCol();
		Direction pacmanDir = pacman.getDirection();
		CommonField dest;
			GhostObject blinky = this.parent.get_maze().ghosts().get(0);
			int diffR = pacmanR - blinky.getRow();
			int diffC = pacmanC - blinky.getCol();
			 targetR = pacmanR + diffR;
			 targetC =  pacmanC + diffC;
			if(targetR > maze.getHeight())
				targetR = maze.getHeight() - 1;
			if(targetC > maze.getWidth())
				targetC = maze.getWidth() - 1;
			if(targetR < 0)
				targetR = 0;
			if(targetC < 0)
				targetC = 0;


			dest = djikstra(targetR, targetC, order);
			if(dest != null){
				maze.getField(targetR, targetC) .debugMark(new Color(0,0,255,200), order);
			}
			else{
				maze.getField(targetR, targetC) .debugMark(new Color(0,0,255,100), order);
				maze.getField(pacmanR, pacmanC) .debugMark(new Color(0,0,255,100), order);
			}
			return dest;
	}

	/**
     * Updates Clyde's target position based on its current position,
     * target direction, and movement strategy.
     *
     * @return the target CommonField for Clyde
     */
	CommonField updateClyde(){
		CommonMaze maze = this.parent.get_maze();
		PacmanObject pacman = maze.getPacManObject();
		int pacmanR = pacman.getRow();
		int pacmanC = pacman.getCol();
		int targetR = pacman.getRow();
		int targetC = pacman.getCol();
		Direction pacmanDir = pacman.getDirection();
		CommonField dest;

			GhostObject blinky = this.parent.get_maze().ghosts().get(2);
			int diffR = (pacmanR - blinky.getRow());
			int diffC = (pacmanC - blinky.getCol());

		switch(pacmanDir){
			case D:
				targetR += 4;
				break;
			case U:
				targetR -= 4;
				break;
			case L:
				targetC -= 4;
				break;
			case R:
				targetC += 4;
				break;
		default:
		}
			if(pacmanR > maze.getHeight() / 2)
				targetR = pacmanR + (diffR * 2) /3;
			else
				targetR = pacmanR - (diffR *2)/3;
			if(pacmanC > maze.getWidth() / 2)
				targetC = pacmanC + (diffC *2)/ 3;
			else
				targetC = pacmanC - (diffC *2)/ 3;

			if(targetR > maze.getHeight())
				targetR = maze.getHeight() - 1;
			if(targetC > maze.getWidth())
				targetC = maze.getWidth() - 1;
			if(targetR < 0)
				targetR = 0;
			if(targetC < 0)
				targetC = 0;
			if(Math.abs(diffC) + Math.abs(diffR) < 4){
				targetR = pacmanR;
				targetC = pacmanC;
			}

			dest = djikstra(targetR, targetC, order);
			if(dest != null){
				maze.getField(targetR, targetC) .debugMark(new Color(255,255,0,200), order);
			}
			else{
				maze.getField(targetR, targetC) .debugMark(new Color(255,255,0,100), order);
				maze.getField(pacmanR, pacmanC) .debugMark(new Color(255,255,0,100), order);
			}


			return dest;
	}


	/**
     * Finds the direction to Pac-Man based on the ghost's current position
     * and movement strategy.
     *
     * @return the direction to move towards Pac-Man
     */
	private Direction findDirToPacman(){
		CommonMaze maze = this.parent.get_maze();
		PacmanObject pacman = maze.getPacManObject();
		int pacmanR = pacman.getRow();
		int pacmanC = pacman.getCol();
		int targetR = pacman.getRow();
		int targetC = pacman.getCol();
		Direction pacmanDir = pacman.getDirection();
		CommonField dest;
		if(order == 0){
				dest = djikstra(pacmanR, pacmanC, order);
		}else
		if(order == 1){
			dest = updatePinky();
		}else if(order == 2){
			dest = updateInky();
		}else if(order == 3){
			dest = updateClyde();
		}else{
				dest = djikstra(pacmanR, pacmanC, order);
		}
		if(dest == null)
				dest = djikstra(pacmanR, pacmanC, order);

		if(dest != null){
			if(dest.get_x() < this.row){
				return Direction.U;
			}else if (dest.get_x() > this.row) {
				return Direction.D;
			}
			if(dest.get_y() < this.col){
				return Direction.L;
			} else if(dest.get_y() > this.col){
				return Direction.R;

			}
		}
		return Direction.STOP;
	}

	private Direction getRandomDir(){
		boolean found_direction = false;

		if (this.counter == 0){
			while (!found_direction){
				Random random = new Random();
				int random_number = random.nextInt(4);

				switch(random_number){
					case 0:
						if (canMove(Direction.U)){
							found_direction = true;
              return Direction.U;
						}
						break;
					case 1:
						if (canMove(Direction.D)){
							found_direction = true;
              return Direction.D;
						}
						break;
					case 2:
						if (canMove(Direction.L)){
							found_direction = true;
              return Direction.L;
						}
					    break;
					case 3:
						if (canMove(Direction.R)){
							found_direction = true;
              return Direction.R;
						}
						break;
				}
			}
		}
		this.counter = (this.counter + 1) % 10;
		return Direction.STOP;
	}

	public void onMoveDown() {
		this.dirImage = 2;
	}

	public void onMoveUp() {
		this.dirImage = 3;
	}

	public void onMoveRight() {
		this.dirImage = 1;
	}

	public void onMoveLeft() {
		this.dirImage = 0;
	}

}
