/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;

import IJA.App.tool.common.CommonField.Direction;
import IJA.App.common.NotYetImplemented;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.view.ComponentView;
import java.awt.Image;
import IJA.App.common.MazeController;


import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import java.util.List;
import java.util.ArrayList;

/**
 * Represents a gate object in the maze. The gate object is placed on a CommonField
 * and has a visual representation using an image file.
 */
public class GateObject implements CommonMazeObject {
	CommonField parent = null;
	int x, y;
	String PID;
	private List<Image> images;
	MazeController controller;

	/**
     * Constructs a new GateObject with the specified parent field.
     * Initializes the gate object's position and loads its image.
     *
     * @param parent the CommonField that the gate object belongs to
     */
	public GateObject(CommonField parent) {
		this.parent = parent;
		this.x = parent.get_x();
		this.y = parent.get_y();
        this.images = new ArrayList<Image>();

		try {
            File file = new File("lib/gate.png");
            images.add(ImageIO.read(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public boolean canMove(CommonField.Direction dir) {
		throw new NotYetImplemented();
	}

	public boolean move() {
		throw new NotYetImplemented();
	}

    public void updatePosition(){
		throw new NotYetImplemented();
	}

	public CommonField getField() {
		throw new NotYetImplemented();
	}


    public void setXY(int x, int y){
		throw new NotYetImplemented();
	}

	public int get_x(){
		return this.x;
	}

	public int get_y(){
		return this.y;
	}

	public List<Image> getImage(){
		return this.images;
	}

	public void setImage(Image image){
		throw new NotYetImplemented();
	}

    public ComponentView getObjView() {
		throw new NotYetImplemented();
	}
	
	public void setObjView(ComponentView obj_view) {
		throw new NotYetImplemented();
	}
	public Direction getDirection(){
		throw new UnsupportedOperationException("Cannot get direction of key");
	}

	public void setDirection(Direction d){
		throw new UnsupportedOperationException("Cannot set direction of key");
	}
	public String getPID(){
		return this.PID;
	}
	public void setPID(String PID){
		this.PID = PID;
	}
	public void setController(MazeController cnt){
		this.controller = cnt;
	}

}
