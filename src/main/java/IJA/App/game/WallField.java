/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;

import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.Observer;

import java.util.TreeMap;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

/**
 * The WallField class represents a wall field in the maze. It implements the
 * CommonField interface and is responsible for managing its position in the maze
 * and notifying observers of changes.
 */
public class WallField implements CommonField {
	CommonMaze maze = null;
	int row, col;
	int num_notifications = 0;
	List<Observer> observers = new ArrayList<>();
	byte neighbor_val = 0;

	/**
     * Constructs a WallField with the specified row and column.
     * @param row the row index of the WallField in the maze
     * @param col the column index of the WallField in the maze
     */
	public WallField(int row, int col) {
		this.row = row;
		this.col = col;
	}

	/**
     * Checks if the specified object is equal to instance WallField.
     * @param obj the object to be compared with instance WallField
     * @return true if the specified object is equal to instance WallField, false otherwise
     */
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof WallField)) {
			return false;
		}
		if (this == obj) {
			return true;
		}

		WallField other = (WallField) obj;
		return other.row == this.row && other.col == this.col;
	}

	public boolean isEmpty() {
		return true;
	}

	public boolean move(CommonField destination, CommonMazeObject object) {
		throw new UnsupportedOperationException("Cannot insert object to Wall field");
	}

	public boolean put(CommonMazeObject object) {
		throw new UnsupportedOperationException("Cannot insert object to Wall field");
	}

	public boolean remove() {
		throw new UnsupportedOperationException("Cannot remove object from Wall field. Wall field cannot contain object");
	}



	public void setMaze(CommonMaze maze) {
		this.maze = maze;
	}

	public CommonField nextField(Direction dir) {
		throw new UnsupportedOperationException("Not implemented for wall field");
	}

	public CommonMazeObject get() {
		throw new UnsupportedOperationException("Not implemented for wall field");
	}

	public boolean contains(CommonMazeObject obj) {
		return false;
	}

	@Override
	public void notifyObservers(){
		for(Observer o: observers){
				this.num_notifications++;
				o.update(this);
			}
	}

	@Override
	public void removeObserver(Observer o){
		observers.remove(o);
	}

	@Override
	public void addObserver(Observer o){
		observers.add(o);
	}

	public void clear_counter_notification(){
		num_notifications = 0;
	}

	public int num_of_notifications(){
		return num_notifications;
	}

	public int get_x(){
		return row;
	}

	public int get_y(){
		return col;
	}

	public CommonMaze get_maze(){
		return this.maze;
	}

	public int GetNumberOfObjects(){
		throw new UnsupportedOperationException("Not implemented for wall field");
	}

	public void setNeighborVal(byte neighbors){
		neighbor_val = neighbors;
	}
	public byte getNeighborVal(){
		return neighbor_val;
	}

	TreeMap<Integer, Color> marks = new TreeMap<Integer, Color>();
	public void debugMark(Color c, int markNum){
		marks.put(markNum, c);
	}
	public void clearMark(int markNum){
		marks.put(markNum, null);
	}
	public TreeMap<Integer, Color> getMarks(){
		return marks;
	}
	public boolean isMarked(){
		for(var mark : marks.values()){
			if(mark != null)
				return true;
		}
		return false;
	}
}
