/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;


import IJA.App.tool.common.CommonField.Direction;
import IJA.App.tool.common.CommonField;
import IJA.App.common.ChangeDirCommand;
import java.awt.Color;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * The PacmanObject class represents the Pac-Man character in the maze.
 * It extends the CommonMovable class and provides specific functionality for Pac-Man.
 */
public class PacmanObject extends CommonMovable {
	int lives, mouseX = 0, mouseY = 0;
	// when true, pacman is controlled by mouse, if false, by keyboard
	boolean mouseControl = false;
	public int dirImage;

	/**
     * Constructs a PacmanObject with the specified parent CommonField.
     * @param parent the CommonField that the PacmanObject is initially located in
     */
	public PacmanObject(CommonField parent) {
		super(parent);
		this.lives = 3;
		this.dirImage = 2; // default
		speed_multiplier = 1.0f;

		try {
			File file = new File("lib/d3.png");
			images.add(ImageIO.read(file));
			file = new File("lib/c3.png");
			images.add(ImageIO.read(file));
			file = new File("lib/a3.png");
			images.add(ImageIO.read(file));
			file = new File("lib/b3.png");
			images.add(ImageIO.read(file));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}



	/**
	 * Check current position, if it's (more or less) in a center of filed, sets new direction to move in
	 *
	 * more or less means +- speed
	 *  */
	public void updateDirection(){
		int x_remainder = this.x % this.objectWidth;
		int y_remainder = this.y % this.objectHeight;

		// check if we're in the center of the tile
		if (x_remainder < speed && y_remainder < speed) {
			this.col = this.x / this.objectWidth;
			this.row = this.y / this.objectHeight;
			this.parent = this.parent.get_maze().getField(this.row, this.col);
			this.parent.visit(true, controller);

			if(mouseControl)
				 updateUsingMouse();

			if ((!canMove(this.nextDirection))){
					this.nextDirection = Direction.STOP;
			}
			// to record only minimal ammount of commands, only execute commands that change something
			if(this.direction != this.nextDirection){
				var cmd = new ChangeDirCommand(this.parent.get_maze(), getPID(), this.nextDirection);
				controller.exec(cmd, this.parent.get_maze());
			}
		}
	}

	/**
	 * Direction update logic when pacman is controlled using keyboard
	 *  */
	void updateUsingMouse(){
		int mouseC = mouseX / this.objectWidth - 0;
		int mouseR = (mouseY - this.objectHeight / 2) / this.objectHeight;

		// check if we're at possition clicked
			if(mouseC ==  this.col && mouseR == this.row){
				mouseControl = false;
				this.nextDirection = Direction.STOP;
			}
			// clear all marks
			for(int i = 0; i < this.parent.get_maze().numRows(); i++){
				for(int j = 0; j < this.parent.get_maze().numCols(); j++){
					var p =	this.parent.get_maze().getField(i,j);
					// ghosts are using marks starting from 0, pacman could use something
					// like 255, but that would introduce arbitrary restriction nobody
					// cares about
					p.clearMark(-1);
				}
			}

			//find path to destination
		var dest = djikstra(mouseR, mouseC, -1);
		//mark destination
		this.parent.get_maze().getField(mouseR, mouseC) .debugMark(new Color(0,255,255,200), -1);

		if(dest != null){
			if(dest.get_x() < this.row){
				this.nextDirection = Direction.U;
			}else if (dest.get_x() > this.row) {
				this.nextDirection = Direction.D;
			}
			if(dest.get_y() < this.col){
				this.nextDirection = Direction.L;
			} else if(dest.get_y() > this.col){
				this.nextDirection = Direction.R;
			}
		}else{
			//coudn't find path to position clicked
				this.nextDirection = Direction.STOP;
		}


	}



	public void onMoveDown() {
		this.dirImage = 0;
	}

	public void onMoveUp() {
		this.dirImage = 3;
	}

	public void onMoveRight() {
		this.dirImage = 2;
	}

	public void onMoveLeft() {
		this.dirImage = 1;
	}


	public int getLives() {
		return this.lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public void damage() {
		this.lives--;
	}


	public boolean isPacman() {
		return true;
	}

	public void setMouseXY(int x, int y){
		mouseX = x;
		mouseY = y;
	}
	public void onMousePressed(){
		mouseControl = true;
	}
	public void onKeyPressed(){
		mouseControl = false;
	}


}
