/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.game;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;

import IJA.App.common.MapParseException;
import IJA.App.common.Maze;
import IJA.App.common.MazeController;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMaze;

/**
 * The MazeConfigure class is responsible for reading, parsing, and configuring a maze from a map or a replay file.
 * It can also create a maze instance based on the parsed data.
 */
public class MazeConfigure {
	int num_lines;
	int num_cols;
	int current_line = 0;
	// is true until invalid line in passed to processLine
	boolean valid;
	CommonField[][] fields;
	List<GhostObject> ghosts;
	PacmanObject PacMan;
	int keys = 0;
	List<String> rawMaze;
	String mapFileName, replayFileName;
	MazeController controller = null;
	boolean loadingReplay;

	/**
     * Constructs a MazeConfigure object by processing the command-line arguments and loading the specified map or replay file.
     * @param args the command-line arguments
     * @throws FileNotFoundException if the specified file is not found
     * @throws IOException if there is an error reading the file
     * @throws ClassNotFoundException if there is a problem deserializing the replay file
     * @throws MapParseException if there is a problem parsing the map file
     */
	public MazeConfigure(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, MapParseException{
		this.mapFileName = null;
		this.replayFileName = null;
		processArgs(args);
		fileChoserIfMapNotSpecified();
		load();
	}

	/**
	 * processes argv from user, sets appropriate member variables to non-null if specified
	 *  */
	void processArgs(String[] args){
		for(int i = 0; i < args.length; i++){
			if(args[i].equals("--replay" ) || args[i].equals("-r"))
				if(i + 1 >= args.length){
				}else{
					this.replayFileName = args[i+1];
					i++;
				}
			if(args[i].equals("--map" ) || args[i].equals("-m"))
				if(i + 1 >= args.length){
				}else{
					this.mapFileName = args[i+1];
					i++;
				}
		}
	}

	/**
	 * checks if map file or replay file was specified if not opens a file choser.
	 *
	 *  */
	void fileChoserIfMapNotSpecified(){
		if(this.mapFileName == null && this.replayFileName == null){
			JFileChooser fc = new JFileChooser();
			int returnVal = fc.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String file = fc.getSelectedFile().getAbsolutePath();
				//This is where a real application would open the file.
				this.mapFileName = file;
			} else {
				this.mapFileName = "data/orig.txt";
			}
		}
	}
	/**
	 * checks if map or replay was specified and loads map or replay accordingly
	 *  */
	void load()throws FileNotFoundException, IOException, ClassNotFoundException, MapParseException{
			controller = null;
		if(this.replayFileName == null){
			loadMap();
		}else{
			loadReplay();
		}
	}

	void loadMap()throws IOException, MapParseException{
		List<String> lines = Files.readAllLines(Paths.get(this.mapFileName));
 int rows, cols;
		try {
			String[] dimensions = lines.get(0).split(" ");
			rows = Integer.parseInt(dimensions[0]);
			cols = Integer.parseInt(dimensions[1]);
		} catch (NumberFormatException e) {
			throw new MapParseException("Could not parse map size in header in " + this.mapFileName);
		}

		startReading(rows, cols);

		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			processLine(line);
		}
		stopReading();
		loadingReplay = false;
	}

	void loadReplay() throws FileNotFoundException, IOException, ClassNotFoundException{
		FileInputStream fis = new FileInputStream(this.replayFileName);
		ObjectInputStream ois = new ObjectInputStream(fis);
		int width = ois.readInt();
		int height = ois.readInt();

		startReading(height, width);
		for (int i = 1; i <= height; i++) {
			String line = (String)ois.readObject();
			processLine(line);
		}
		stopReading();

		controller = (MazeController)ois.readObject();
		controller.resumeAfterLoad();
		ois.close();
		loadingReplay = true;
	}

	public boolean isReplay(){
		return this.loadingReplay;
	}
	public MazeController getController(){
		return this.controller;
	}

	/**
     * Creates a new maze instance based on the parsed map data.
     * @return a CommonMaze instance or null if the maze data is invalid
     */
	public CommonMaze createMaze() {
		// checks if bottom right field is initialized
		if (fields[num_lines - 1][num_cols - 1] == null || valid == false) {
			valid = false;
			return null;
		} 
		// initialize all fields
		
		Maze maze = new Maze(this.fields, this.ghosts, this.PacMan, this.keys);
		for (CommonField[] line : fields) {
			for (CommonField cell : line) {
				cell.setMaze(maze);
			}
		}

		maze.index();
		maze.setHeightWidth(this.num_lines-2, this.num_cols-2);
		maze.setSource(rawMaze);

		return maze;
	}

	public boolean processLine(String line) {

		this.fields[current_line][0] = new WallField(current_line, 0);
		this.fields[current_line][this.num_cols - 1] = new WallField(current_line, this.num_cols - 1);
		this.rawMaze.add(line);

		for (int i = 1; i < line.length() + 1; i++) {
			var PID = String.format("obj%d.%d", current_line, i);
			// check boundries - if line is wider than the maze - invalid input
			if (i >= num_cols) {
				this.valid = false;
				return false;
			}
			PathField field;
			switch (line.charAt(i - 1)) {
			case '.':
				this.fields[current_line][i] = new PathField(current_line, i);
				break;
			case 'X':
				this.fields[current_line][i] = new WallField(current_line, i);
				break;
			case 'G':
				field = new PathField(current_line, i);
				GhostObject ghost = new GhostObject(field, this.ghosts.size());
				ghost.setPID(PID);
				this.ghosts.add(ghost);
				// field.put(ghost);
				this.fields[current_line][i] = field;
				break;
			case 'S':
				field = new PathField(current_line, i);
				var pac = new PacmanObject(field);
				this.PacMan = pac;
				pac.setPID(PID);
				this.fields[current_line][i] = field;
				break;
			case 'K':
				field = new PathField(current_line, i);
				var key = new KeyObject(field);
				key.setPID(PID);
				field.put(key);
				this.fields[current_line][i] = field;
				this.keys++;
				break;
			case 'T':
				field = new PathField(current_line, i);
				var gate =new GateObject(field);
				gate.setPID(PID);
				field.put(gate);
				this.fields[current_line][i] = field;
				break;
			default:
				break;
			}
			if(this.fields[current_line][i] == null){
				System.err.printf("invalid symbol on line %d %d%n", current_line, i);
				System.exit(2);
			}
		}
		// if `line` was shorter than the maze last will be null -> invlaid input
		if (fields[current_line][num_cols - 1] == null) {
			this.valid = false;
			this.current_line += 1;
			return false;
		}
		this.current_line += 1;
		return true;
	}

	public void startReading(int rows, int cols) {
		this.valid = true;
		this.num_cols = cols + 2;
		this.num_lines = rows + 2;
		this.current_line = 1;
		this.fields = new CommonField[rows + 2][cols + 2];
		this.ArtificialWall();
		this.ghosts = new ArrayList<GhostObject>();
		this.rawMaze = new ArrayList<String>();
	}

	// add walls to the sides of the map
	private void ArtificialWall(){
		for (int i = 0; i < this.num_cols; i++){
			this.fields[0][i] = new WallField(0, i);
		}
		for (int i = 0; i < this.num_cols; i++){
			this.fields[this.num_lines - 1][i] = new WallField(this.num_lines - 1, i);
		}
	}

	public boolean stopReading() {
		return valid && fields[fields.length - 1][0].isEmpty();
	}

}
