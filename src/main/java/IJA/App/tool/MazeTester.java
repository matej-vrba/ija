/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool;

import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.common.NotYetImplemented;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.Observer;
import IJA.App.tool.common.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

public class MazeTester {
	HashMap<CommonField, obs> observers;

		class obs implements Observer{

			public boolean updated = false;
			public void update(Observable o){
				this.updated = true;
			}
		}

	// Konstruktor inicializující MazeTester.
	public MazeTester(CommonMaze maze) {
		this.observers = new HashMap<CommonField, obs>();

		int width = maze.numCols();
		int height = maze.numRows();

		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				this.observers.put(maze.getField(i, j), new obs());
				maze.getField(i, j).addObserver(this.observers.get(maze.getField(i, j)));
			}
		}

	}

	// Ověří, že žádné políčko nebylo notifikováno.
	public boolean checkEmptyNotification() {
		for(var o : this.observers.entrySet()){
			if(o.getValue().updated){
				return false;
			}
		}
		return true;
	}


	// Ověří správný průběh notifikace při přesunu objektu mezi políčky - tato
	// políčka generují notifikace o změnách.
	public boolean checkNotification(StringBuilder msg,
																	 CommonMazeObject obj,
																	 CommonField current,
																	 CommonField previous) {
		if(this.observers.get(current).updated == false){
			msg.append("Object current wasn't updated");
			return false;
		}
		if(this.observers.get(previous).updated == false){
			msg.append("Object next wasn't updated");
			return false;
		}
		this.observers.get(current).updated = false;
		this.observers.get(previous).updated = false;
		return true;
	}
}
