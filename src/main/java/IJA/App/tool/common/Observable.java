/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.common;

/**
 * The Observable interface represents a subject in the Observer design pattern.
 * It provides methods for adding, removing, and notifying observers.
 */
public interface Observable {
	/**
     * Adds an observer to the list of observers that will be notified of changes in the observable.
     * @param o the observer to be added
     */
	void addObserver(Observer o);

	/**
     * Notifies all registered observers of changes in the observable.
     */
	void notifyObservers();

	/**
     * Removes an observer from the list of observers that will be notified of changes in the observable.
     * @param o the observer to be removed
     */
	void removeObserver(Observer o);
}
