/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.common;

/**
 * The Observer interface represents an observer in the Observer design pattern.
 * It defines a method for processing notifications about changes in an Observable object.
 */
public interface Observer {
	
	/**
     * Processes the notification of a change in the Observable object.
     * @param o the Observable object that has changed
     */
	void update(Observable o);
}