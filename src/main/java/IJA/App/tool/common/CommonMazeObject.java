/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.common;
import java.awt.Image;
import java.util.List;
import IJA.App.tool.common.CommonField.Direction;
import IJA.App.common.MazeController;

public interface CommonMazeObject {
	// Ověří, zda je možné přesunout objekt zadaným směrem.
	boolean canMove(CommonField.Direction dir);

	// Vrátí objekt políčka, na kterém je objekt umístěn.
	CommonField getField();

	// Vrátí aktuální počet životů objektu.
	// int getLives();

	// Ověřuje, zda objekt reprezentuje panáčka Pacman.
	default boolean isPacman() {
		return false;
	}

	// Přesune objekt na pole v zadaném směru, pokud je to možné.
	boolean move();

	void setXY(int x, int y);

	int get_x();

	int get_y();

	List<Image> getImage();

	void setImage(Image image);

	void updatePosition();

	Direction getDirection();

	 void setDirection(Direction d);

	default void setDisabled(boolean d){
		throw new UnsupportedOperationException("disable not implemented");
	}

	default boolean isDisabled(){
		return false;
	}


	/**
	 * Returns persistent object ID.
	 *
	 * because objects need to get saved to disk and loaded back, we need some
	 * sort of persistent ID.
	 *
	 * @return Persistent ID of the object
	 *
	 *  */
	String getPID();

	/**
	 * Sets persistent object ID.
	 *
	 * because objects need to get saved to disk and loaded back, we need some
	 * sort of persistent ID.
	 *
	 *  */
	void setPID(String PID);
	void setController(MazeController cnt);
}
