/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.common;

import java.util.List;
import IJA.App.game.GhostObject;
import IJA.App.game.PacmanObject;
import java.io.ObjectOutputStream;
import java.io.IOException;

public interface CommonMaze {

	/**
	 * Returns the CommonField at the specified row and column.
	 * 
	 * @param row the row index of the field
	 * @param col the column index of the field
	 * @return the field at the specified location
	 */
	CommonField getField(int row, int col);

	// Vrátí seznam všech duchů v bludišti.
	List<GhostObject> ghosts();

	/**
	 * Returns the number of columns in the maze.
	 * 
	 * @return the number of columns
	 */
	int numCols();

	/**
	 * Returns the number of rows in the maze.
	 * 
	 * @return the number of rows
	 */
	int numRows();

	PacmanObject getPacManObject();

	void setHeightWidth(int height, int width);

	int getHeight();

	int getWidth();

	/**
	 * Clamps the input value to the range [0, maze width].
	 * Ensures that the returned value lies within the valid x-coordinate range of
	 * the maze.
	 *
	 * @param input the x-coordinate value to be clamped
	 * @return the clamped x-coordinate value
	 */
	int clampX(int input);

	/**
	 * Clamps the input value to the range [0, maze height].
	 * Ensures that the returned value lies within the valid y-coordinate range of
	 * the maze.
	 *
	 * @param input the y-coordinate value to be clamped
	 * @return the clamped y-coordinate value
	 */
	int clampY(int input);

	int getKeys();

	void setKey(int num);

	/**
	 * Indexes objects (creates PID:Object map).
	 *
	 * Indexing should happen exactly once, after creation.
	 */
	void index();

	/**
	 * Returns Object of corresponding PID, Maze must be indexed first.
	 *
	 * @return object with coresponding PID or null of said object doesn't exist
	 */
	CommonMazeObject getByPID(String PID);

	boolean showMarks();

	void toggleMarks();

	/**
	 * Serializes the maze to the provided ObjectOutputStream.
	 * The method writes the source list's length and size, followed by each line in
	 * the source list.
	 *
	 * @param out the ObjectOutputStream to write the serialized maze data to
	 * @throws IOException if an I/O error occurs while writing to the
	 *                     ObjectOutputStream
	 */
	void serialize(ObjectOutputStream out) throws IOException;

	void setSource(List<String> src);
}
