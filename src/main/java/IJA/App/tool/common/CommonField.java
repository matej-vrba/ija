/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.common;
import IJA.App.common.MazeController;
import java.awt.Color;
import java.util.TreeMap;


public interface CommonField extends Observable {
	public static enum Direction {
		D, L, R, U, STOP,
	}


	boolean contains(CommonMazeObject obj);

	CommonMazeObject get();

	boolean isEmpty();

	CommonField nextField(CommonField.Direction dirs);

	void setMaze(CommonMaze maze);

	// boolean move(CommonField destination, CommonMazeObject object);

	public void clear_counter_notification();

	public int num_of_notifications();

	public int get_x();

	public int get_y();

	public CommonMaze get_maze();

	boolean remove();

	void setNeighborVal(byte neighbors);

	byte getNeighborVal();
	default void visit(boolean visited, MazeController controller){
		throw new UnsupportedOperationException("setVisited not implemented");
	}
	default void setVisited(boolean visited){
		throw new UnsupportedOperationException("setVisited not implemented");
	}
	default boolean visited(){
		return false;
	}

	boolean isMarked();
	void debugMark(Color c, int markNum);
	void clearMark(int markNum);
	TreeMap<Integer, Color> getMarks();

}
