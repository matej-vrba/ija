/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool;


import IJA.App.tool.common.CommonMaze;
import IJA.App.common.MazeController;
import IJA.App.common.HurtCommand;
import IJA.App.game.GateObject;
import IJA.App.game.GhostObject;
import IJA.App.game.KeyObject;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonField.Direction;
import IJA.App.tool.view.FieldView;
import IJA.App.tool.view.GhostView;
import IJA.App.tool.view.PacmanView;
import java.util.Random;
import java.io.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import javax.imageio.ImageIO;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.Image;
import java.util.List;
import java.util.ArrayList;
import java.awt.Graphics;
import IJA.App.game.PacmanObject;
import IJA.App.game.WallField;

/**
 * MazePresenter represents a visual representation of the maze.
 * It handles the rendering of the maze, its elements, and manages the game state and user interactions.
 */
public class MazePresenter extends JPanel implements ComponentListener{
	CommonMaze maze = null;
	int height = 1000; 
	int width = 1000; 
	JFrame frame = new JFrame();
	public Timer sharedTimer;
	private Thread updateThread;
	private FieldView[][] fieldViews;
	private int cooldDownCounter = 0;
	private static final int coolDownDuration = 200;
	private boolean end = false;
	private MazeController controller = null;
	Image heartImage = null;
	Image lockImage = null;
	// when stepping this saves number of steps since replay was paused
	int nofSteps = 0;
	ArrayList<Image> mazeSprites = new ArrayList<Image>();
	Font font = new Font("Arial", Font.PLAIN, 20);

	static final int TARGET_FPS = 60;

	// Text on the bottom of the screen
	String statusBar = "Loaded";
	// variables controlling how replay works.
	// play - when false, game is paused
	// replay - when true, replaying from saved game
	// forward - when false, replaying backwards
	boolean replay = false, forward = true, step = false, play = true;

	/**
     * Constructor for initializing the MazePresenter.
     *
     * @param maze         the CommonMaze object to be rendered
     * @param loadingReplay a boolean flag indicating whether the presenter is loading from a saved replay
     * @param controller   the MazeController responsible for controlling the game state
     */
	public MazePresenter(CommonMaze maze, boolean loadingReplay, MazeController controller){
		this.maze = maze;
		if(controller == null){
			this.controller = new MazeController(maze);
		}else{
			this.controller = controller;
		}

		// if we're loading from saved replay, go straight into replay mode
		this.replay = loadingReplay;

		loadAssets();
		initMovables();
		initFields();
		initWalls();

		this.addComponentListener(this);
		createUpdateThread();
	} // MazePresenter

	/**
     * Loads an image from a given file path.
     *
     * @param path the file path of the image to load
     * @return the loaded Image object, or null if the image could not be loaded
     */
	Image loadImg(String path){
		try {
			 return ImageIO.read(new File(path));
		} catch (IOException e) {
			System.err.println(String.format("Failed to read image " + path));
			System.err.println(e);
			System.exit(1);
			return null;
		}
	}

	/**
     * Loads the assets needed for rendering game elements.
     */
	void loadAssets(){
		heartImage = loadImg("lib/heart.png");
		lockImage = loadImg("lib/lock.png");
		for(int i = 0; i < 41; i++)
			mazeSprites.add(loadImg(String.format("lib/w%d.png", i + 1)));
		mazeSprites.add(loadImg("lib/rosekane_12.png"));
	}

	/**
	 * Creates update thread, this thread will be started in function `open`
	 *  */
	void createUpdateThread(){

		this.sharedTimer = new Timer(1000 / TARGET_FPS, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (updateThread == null || !updateThread.isAlive()) {
						updateThread = new Thread(new Runnable() {
								@Override
								public void run() {
									update();
								}
							});
						updateThread.start();
					}
				}
			});
	}

	/**
	 * Initializes path fields and wall fields
	 *  */
	void initFields(){
		int rows = this.maze.numRows();
		int cols = this.maze.numCols();
		this.fieldViews = new FieldView[rows][cols];
		//loop through all fields
		for (int x = 0; x < rows; x++) {
			for (int y = 0; y < cols; y++) {
				CommonField field = this.maze.getField(x, y);
				//create field view for every fields
				FieldView o = new FieldView(field, this);
				field.addObserver(o);
				this.fieldViews[x][y] = o;
				//give all objects reference to maze controller
				if (!field.isEmpty()){
					var obj = field.get();
					obj.setController(this.controller);
				}
			}
		}
	}

	/**
	 * Initializes ghosts and pacman
	 *  */
	void initMovables(){
			PacmanObject pacman = this.maze.getPacManObject();
			// speed is how fast object should move on screen, but size of maze tile
			// is dependent on size of maze and screen
			int colWidth = this.width / this.maze.numCols();
			int colHeight = this.height / this.maze.numRows();

			pacman.setXY(colWidth * this.maze.getPacManObject().getCol(),
									 colHeight * this.maze.getPacManObject().getRow());
			pacman.setObjectWidthHeight(colWidth, colHeight);

			List<GhostObject> Ghosts = this.maze.ghosts();
			for (GhostObject ghost : Ghosts) {
				ghost.setXY(colWidth * ghost.getCol() ,
										colHeight * ghost.getRow());
				ghost.setObjectWidthHeight(colWidth, colHeight);
			}

			pacman.setObjView(new PacmanView(pacman));
			pacman.setController(this.controller);

			List<GhostObject> ghost_objects = this.maze.ghosts();
			for (GhostObject ghost : ghost_objects) {
				ghost.setObjView(new GhostView(ghost));
				ghost.setController(this.controller);
			}
	}

	/**
	 * Initializes evaluation of neighbors, used during rensering to select
	 * correct sprite based on neighbors.
	 *
	 * The evaluation is a bit field where every bit stand for one of 8 neighbors
	 *
	 *
	 *  */
	void initWalls(){
			int rows = this.maze.numRows();
			int cols = this.maze.numCols();
			byte neighbors;
			for (int x = 0; x < rows; x++){
				for (int y = 0; y < cols; y++){
					//create bitfields of neighbors, where 1 is wall and 0 is path
					neighbors = 0;
					neighbors += (x   == 0    || y == 0      ? 1 : (this.maze.getField(x-1, y-1)instanceof WallField? 1 : 0  )) << 7;
					neighbors += (x   == 0                   ? 1 : (this.maze.getField(x-1, y  )instanceof WallField? 1 : 0  )) << 6;
					neighbors += (x   == 0    || y+1 >= cols ? 1 : (this.maze.getField(x-1, y+1)instanceof WallField? 1 : 0  )) << 5;
					neighbors += (y   == 0                   ? 1 : (this.maze.getField(x,   y-1)instanceof WallField? 1 : 0  )) << 4;
					neighbors += (y+1 >= cols                ? 1 : (this.maze.getField(x,   y+1)instanceof WallField? 1 : 0  )) << 3;
					neighbors += (x+1 >= rows || y == 0      ? 1 : (this.maze.getField(x+1, y-1)instanceof WallField? 1 : 0  )) << 2;
					neighbors += (x+1 >= rows                ? 1 : (this.maze.getField(x+1, y  )instanceof WallField? 1 : 0  )) << 1;
					neighbors += (x+1 >= rows || y+1 >= cols ? 1 : (this.maze.getField(x+1, y+1)instanceof WallField? 1 : 0  )) << 0;

					this.maze.getField(x, y).setNeighborVal(neighbors);
				}
			}
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int rows = this.maze.numRows();
		int cols = this.maze.numCols();
		for (int x = 0; x < rows; x++){
			for (int y = 0; y < cols; y++){
				var neighbor_val = this.maze.getField(x,y).getNeighborVal();
					this.fieldViews[x][y].paintComponent(g, neighbor_val,mazeSprites);
			}
		}
		this.maze.getPacManObject().getObjView().paintComponent(g);

		List<GhostObject> ghost_objects = this.maze.ghosts();
		for (GhostObject ghost : ghost_objects) {
			ghost.getObjView().paintComponent(g);
		}

		paintUI(g);
	}

	void paintUI(Graphics g){
		int lives = this.maze.getPacManObject().getLives();
		int keys = this.maze.getKeys();

		if (this.end){
			if (this.maze.getPacManObject().getLives() <= 0){
				Random random = new Random();
				int r_num = random.nextInt(3);

				switch(r_num){
				case 0:
					statusBar = "LOST !!!! Better luck next time";
					break;
				case 1:
					statusBar = "You lost! Sounds like skill issue to me";
					break;
				case 2:
					statusBar = "You lost! git gud";
					break;
				}
			}
			else {
				statusBar = "CONGRATULATIONS YOU HAVE WON!!!";
			}
		}
		if (this.maze.getKeys() == 0){
			statusBar = "Find the gate";
		}


		g.setColor(Color.BLACK);
		g.setFont(font);

		for(int i = 0; i < lives; i++)
			g.drawImage(heartImage, 10 + i*33, 5, 32, 32, null);
		for(int i = 0; i < keys; i++)
			g.drawImage(lockImage, 200 + i*33, 5, 32, 32, null);
		var height = frame.getContentPane().getSize();
		g.drawString(statusBar, 0, (int)height.getHeight() - 5);
	}

		public CommonMaze getMaze(){
			return this.maze;
		}

		//Vytvoří a otevře GUI.
		public void open(){
			frame.addKeyListener(createGameKeyListener());
			frame.addMouseListener(new PacmanMouseListener());


			frame.add(this, BorderLayout.CENTER);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setTitle("Pac-Man");
			frame.pack();
			frame.setSize(width, height);
			frame.setVisible(true);
			frame.setResizable(false);

			sharedTimer.start();
		}

		public void update(){
			PacmanObject pacman = this.maze.getPacManObject();
			// in regular mode steps time forward, in replay mode steps time
			// forward/backward and excutes/undoes recorded commands
			if(!play)
				return;
			updateTime();

			// most of the logic is(or should be) saved in commands, so most of the
			// game logic is undesired
			if(!replay){
				updateDirs();
				// NOTE: colide checks collision with other objects (ghosts, keys,
				// gate) NOT with wall, collision with walls is being checked in move
				// methods
				colide();

				if (pacman.getLives() <= 0){
					//this.sharedTimer.stop();
					this.repaint();
					this.end = true;
					this.replay = true;
					this.play = false;
					return;
				}
			}

		}

		private GameKeyListener createGameKeyListener(){
			return new GameKeyListener();
		}

		public int get_width(){
			return this.width;
		}

		public int get_height(){
			return this.height;
		}

	private void updateTime(){
		PacmanObject pacman = this.maze.getPacManObject();

		// in replay mode we want to step in time (and after we're at beginning/end)
		// to stop
		// in normal mode, we want to step time forward

		if(replay){
			int num;
			// using different methods for stepping forward/backward, but they behave
			// the same (return number of executed/undone commands, -1 at beginning/end)
			if(forward)
				num = controller.replay(maze);
			else
				num = controller.undo(maze);
			if(num >= 0){
				repaint();
				if(num > 0 && step && nofSteps > 10){
					play = false;
					nofSteps = 0;
				}
				nofSteps++;
			}
			else{
				return;
			}
		}else{
			this.controller.step();
		}

			updatePositions();
	}

	void setTimeForward(boolean forward){
		if(forward != this.forward) {
			var pacman = maze.getPacManObject();
			this.forward = !this.forward;
			pacman.reverseSpeed();
			for(var ghost: maze.ghosts())
				ghost.reverseSpeed();
		}
	}

	private void updatePos(){
		for(var ghost: this.maze.ghosts())
			ghost.move();
		this.maze.getPacManObject().move();
	}

	/**
	 * Update directions in which are pacman and ghosts moving
	 * */
	private void updateDirs(){
			for (GhostObject ghost : this.maze.ghosts())
				ghost.updateDirection();
			this.maze.getPacManObject().updateDirection();
	}

	/**
	 * Checks collisions with static objects (keys, gate) and ghosts
	 *  */
	private void colide(){
		var pacman = this.maze.getPacManObject();
		int xDistFromFieldCenter = pacman.get_x() % pacman.getObjectWidth();
		int yDistFromFieldCenter = pacman.get_y() % pacman.getObjectHeight();

		// check if we're more or less in a center of the field
		if (xDistFromFieldCenter < pacman.getSpeed() && yDistFromFieldCenter < pacman.getSpeed()) {
			int col = pacman.get_x() / pacman.getObjectWidth();
			int row = pacman.get_y() / pacman.getObjectHeight();

			CommonField field = this.maze.getField(row, col);
			if (!field.isEmpty()){
				CommonMazeObject obj = field.get();
				if (obj instanceof KeyObject){
					KeyObject keyObj = (KeyObject)obj;
					if(!keyObj.isDisabled()){
						keyObj.disable();
						this.fieldViews[row][col].update(this.maze.getField(row, col));
						statusBar = "Key picked up";
					}
				}
				else if (obj instanceof GateObject){
					if (this.maze.getKeys() == 0){
						//this.sharedTimer.stop();
						this.repaint();
						this.end = true;
						this.replay = true;
						this.play = false;
						return;
					}
				}
			}
			}
		List<GhostObject> ghost_list = this.maze.ghosts();
		for (GhostObject ghost : ghost_list)
			checkCollision(ghost, pacman);
	}

		private void updatePositions() {
			List<GhostObject> Ghosts = this.maze.ghosts();
			for (GhostObject ghost : Ghosts)
				ghost.move();

			this.maze.getPacManObject().move();
			this.repaint();
		}

		private void checkCollision(GhostObject ghost, PacmanObject pacman){
			if (this.cooldDownCounter > 0){
				this.cooldDownCounter--;
				return;
			}

			int threshold = (int) Math.round(0.8 * Math.min(pacman.getObjectWidth(), pacman.getObjectHeight()));

			int dx = pacman.get_x() - ghost.get_x();
			int dy = pacman.get_y() - ghost.get_y();
			int distance = (int) Math.sqrt(dx *dx + dy *dy);

			if (distance < threshold){
				var cmd = new HurtCommand(pacman.getPID());
				controller.exec(cmd, maze);
				this.cooldDownCounter = this.coolDownDuration;
				statusBar = "PacMan damaged";
			}
		}

		@Override
		public void componentHidden(ComponentEvent arg0) {
			// do nothing
		}

		@Override
		public void componentMoved(ComponentEvent arg0) {
			// do nothing
		}

		@Override
		public void componentResized(ComponentEvent arg0) {
			this.validate();
		}

		@Override
		public void componentShown(ComponentEvent arg0) {
			// do nothing
		}

	class PacmanMouseListener implements MouseListener{
		//Invoked when the mouse button has been clicked (pressed and released) on a component.
		public void mouseClicked(MouseEvent e){

		}
		//Invoked when the mouse enters a component.
		public void mouseEntered(MouseEvent e){

		}
		//Invoked when the mouse exits a component.
		public void mouseExited(MouseEvent e){

		}
		//Invoked when a mouse button has been pressed on a component.
		public void mousePressed(MouseEvent e){
			switch(e.getButton()){
				case MouseEvent.BUTTON1:
					maze.getPacManObject().setMouseXY(e.getX(), e.getY());
					maze.getPacManObject().onMousePressed();
					default:
			}
		}
		//Invoked when a mouse button has been released on a component.
		public void mouseReleased(MouseEvent e){

		}

	}// class PacmanMouseListener

	private class GameKeyListener implements KeyListener {
		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			var pacman = maze.getPacManObject();
			switch (keyCode) {
					// Move up
				case KeyEvent.VK_W:
				case KeyEvent.VK_UP:
					maze.getPacManObject().setNextDir(Direction.U);
					maze.getPacManObject().onKeyPressed();
					return;
					// Move left
				case KeyEvent.VK_A:
				case KeyEvent.VK_LEFT:
					maze.getPacManObject().setNextDir(Direction.L);
					maze.getPacManObject().onKeyPressed();
					return;
					// Move down
				case KeyEvent.VK_S:
				case KeyEvent.VK_DOWN:
					maze.getPacManObject().setNextDir(Direction.D);
					maze.getPacManObject().onKeyPressed();
					return;
					//move right
				case KeyEvent.VK_D:
				case KeyEvent.VK_RIGHT:
					maze.getPacManObject().setNextDir(Direction.R);
					maze.getPacManObject().onKeyPressed();
					break;
					// Try to replay
				case KeyEvent.VK_R:
					reverseReplay();
					break;

				case KeyEvent.VK_END:
					saveReplay();
					break;
				case KeyEvent.VK_SPACE:
					//enable/disable stepping
					toggleReplayPlay();
					break;
				case KeyEvent.VK_I:
					toggleStepping();
					break;
				case KeyEvent.VK_F1:
						statusBar = "F1-Help WSAD-Moving Space-pause I-Stepping End-Save replay R-Reverse K/L - forward, H/J - backward";
					if(!play) repaint();
					break;
				case KeyEvent.VK_F11:
						statusBar = "Marks toggled";
						maze.toggleMarks();
					if(!play) repaint();
					break;
				case KeyEvent.VK_H:
					replayGotoStart();
					break;
				case KeyEvent.VK_L:
					replayGotoEnd();
					break;
				case KeyEvent.VK_K:
					replayStepForward();
					break;
				case KeyEvent.VK_J:
					replayStepBack();
					break;
			}
		}// void keyPressed(KeyEvent e)

		@Override
		public void keyReleased(KeyEvent e) {
			// Handle key release events if needed
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// Handle key typed events if needed
		}
		
	} // class GameKeyListener

	void reverseReplay(){
		replay = true;
		setTimeForward(!forward);
		statusBar = "Reversing";
		if(!play) repaint();
	}

	void saveReplay(){
		try (ObjectOutputStream out =
				 new ObjectOutputStream(new FileOutputStream("replay.dat"))) {
			maze.serialize(out);
			out.writeObject(controller);
		} catch (Exception ee) {
			System.err.println("Failed to save replay to file");
			System.err.println(ee);
			System.exit(1);
		}
		statusBar = "Saved replay into ./replay.dat";
		if(!play) repaint();
	}

	void toggleReplayPlay(){
		play = !play;
		if(play)
			statusBar = "Resuming";
		else {
			statusBar = "Paused";
			repaint();
		}
	}
	void toggleStepping(){
		step = !step;
		replay = true;
		if(step)
			statusBar = "Stepping enabled";
		else
			statusBar = "Stepping disabled";
		if(!play) repaint();
	}
	void replayGotoStart(){
		setTimeForward(false);
		while(controller.undo(maze) >= 0)
			updatePositions();
		if(!controller.headAtStart())
			updatePositions();
	}
	void replayGotoEnd(){
		setTimeForward(true);
		while(controller.replay(maze) >= 0)
			updatePositions();
		if(controller.headBehind())
			updatePositions();
	}
	void replayStepBack(){
		setTimeForward(false);
		while(controller.undo(maze) == 0)
			updatePositions();
		if(!controller.headAtStart())
			updatePositions();
	}
	void replayStepForward(){
		setTimeForward(true);
		while(controller.replay(maze) == 0)
							updatePositions();
		if(controller.headBehind())
			updatePositions();
	}
}// class MazePresenter
