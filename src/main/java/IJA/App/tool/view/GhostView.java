/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.view;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;

import java.util.List;
import IJA.App.game.GhostObject;

/**
 * GhostView class is responsible for rendering a ghost object in the maze.
 * It implements the ComponentView interface.
 */
public class GhostView implements ComponentView{

	GhostObject object = null;

	/**
     * Constructor for the GhostView class.
     * @param m the GhostObject this view will represent
     */
	public GhostView(GhostObject m){
			this.object = m;		
	}
	
	public void paintComponent(Graphics g){
		int width = this.object.getObjectWidth();
		int height = this.object.getObjectHeight();

		List<Image> images = object.getImage();

		Image image = images.get(this.object.dirImage);

		if (image != null){
			g.drawImage(image, object.get_x(), object.get_y(), width - 4, height - 4, null);
		}
		else {
			g.setColor(new Color(0, 53, 102));
			g.fillOval(object.get_x(), object.get_y(), width - 4, height - 4);
		}
		
	}

	public void setX(int x) {
        throw new UnsupportedOperationException("Not implemented for GhostView");
    }
    
    public void setY(int y) {
        throw new UnsupportedOperationException("Not implemented for GhostView");
    }
    
    public void setWidth(int width) {
        throw new UnsupportedOperationException("Not implemented for GhostView");
    }
    
    public void setHeight(int height) {
        throw new UnsupportedOperationException("Not implemented for GhostView");
    }
}
