/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.view;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;

import java.util.List;

import IJA.App.tool.common.CommonMazeObject;

/**
 * GateView class is responsible for rendering a gate object in the maze.
 * It implements the ComponentView interface.
 */
public class GateView implements ComponentView{

	FieldView parent = null;
	CommonMazeObject object = null;
    private int x;
    private int y;
    private int width;
    private int height;

	/**
     * Constructor for the GateView class.
     *
     * @param parent the FieldView object that contains this gate view
     * @param m the CommonMazeObject this view will represent
     */
	public GateView(FieldView parent, CommonMazeObject m){
		this.parent = parent;
		this.object = m;
	}

	public void paintComponent(Graphics g){

		List<Image> images = object.getImage();

        Image image = images.get(0);

		if (image != null){
			g.drawImage(image, x + 2, y + 2, width - 4, height - 4, null);
		}
		else {
			g.setColor(new Color(0, 53, 102));
			g.fillOval(x + 2, y + 2, width - 4, height - 4);
		}
		
	}

    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public void setWidth(int width) {
        this.width = width;
    }
    
    public void setHeight(int height) {
        this.height = height;
    }
}
