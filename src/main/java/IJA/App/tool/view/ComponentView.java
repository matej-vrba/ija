/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.view;

import java.awt.Graphics;

/**
 * ComponentView interface defines the methods needed for rendering a graphical
 * component.
 */
public interface ComponentView {
	/**
	 * Renders the graphical representation of the object in the provided Graphics context.
	 * @param g the Graphics context where the object should be rendered
	 */
	void paintComponent(Graphics g);

	void setX(int x);

	void setY(int y);

	void setWidth(int width);

	void setHeight(int height);
}
