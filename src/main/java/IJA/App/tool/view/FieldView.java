/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.view;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;

import IJA.App.tool.MazePresenter;
import IJA.App.tool.common.CommonField;
import IJA.App.tool.common.CommonMazeObject;
import IJA.App.tool.common.CommonMaze;
import IJA.App.tool.common.Observable;
import IJA.App.tool.common.Observer;
import IJA.App.game.PathField;
import IJA.App.game.WallField;
import IJA.App.game.GateObject;
import IJA.App.game.KeyObject;
import java.util.List;



/**
 * FieldView class is responsible for representing and rendering a single field in the maze.
 * It extends JPanel and implements the Observer interface to get updates from the Observable object.
 */
public class FieldView extends JPanel implements Observer {


	CommonField field = null;
	MazePresenter presenter = null;
	ComponentView obj_view = null;

	/**
     * Constructor for the FieldView class.
     *
     * @param f the CommonField object this view will represent
     * @param p the MazePresenter that manages this view
     */
	public FieldView(CommonField f, MazePresenter p) {
		this.field = f;
		this.presenter = p;
		this.update(f);
	}

	 /**
     * Resets the counter for the number of notifications received from the Observable object.
     */
	public void clearChanged() {
		this.field.clear_counter_notification();
	}

	/**
     * Returns the field object represented by this view.
     * @return the CommonField object
     */
	public CommonField getField() {
		return this.field;
	}

	/**
     * Returns the number of notifications received from the Observable object.
     * @return the number of updates received
     */
	public int numberUpdates() {
		return this.field.num_of_notifications();
	}


	/**
     * Renders the field and its content in the given Graphics context.
     * @param g the Graphics object to draw the field on
     * @param neighbors the byte value representing the field's neighbors
     * @param images the list of Image objects used for drawing the field
     */
	public void paintComponent(Graphics g, byte neighbors, List<Image> images) {

		super.paintComponent(g);
		CommonMaze maze = field.get_maze();
		int num_rows = maze.numRows();
		int num_cols = maze.numCols();

		int width = Math.round(this.presenter.get_width() / num_cols);
		int height = Math.round(this.presenter.get_height() / num_rows);
		Image img = null;

		int x = Math.round(this.field.get_y() * width);
		int y = Math.round(this.field.get_x() * height);
		// set color for path/wall
		if (this.field instanceof PathField) {
			g.setColor(new Color(254, 250, 224));
			if(!this.field.visited())
			img = images.get(41);
		} else if (field instanceof WallField) {
			g.setColor(new Color(254, 250, 224));
//
//         neighbor val is bitfield of 8 neighbors, mask is about what neighbors
//         we care and sprite value is value that must match neighborval & mask
//
//         for better explanation see commit "feat: improve rendering" (70bcf92)
//
//         neighbor val    mask           sprite value      sprite
			if     ((neighbors & 0b01011011) == 0b00001011){img = images.get(0);}
			else if((neighbors & 0b01011111) == 0b00011111){img = images.get(1);}
			else if((neighbors & 0b01011110) == 0b00010110){img = images.get(2);}
			else if((neighbors & 0b01011010) == 0b00000010){img = images.get(3);}
			else if((neighbors & 0b11111111) == 0b11111110){img = images.get(4);}
			else if((neighbors & 0b11111111) == 0b11111010){img = images.get(5);}
			else if((neighbors & 0b11111111) == 0b11111011){img = images.get(6);}
			else if((neighbors & 0b01111011) == 0b01101011){img = images.get(7);}
			else if((neighbors & 0b11111111) == 0b11111111){img = images.get(8);}
			else if((neighbors & 0b11011110) == 0b11010110){img = images.get(9);}
			else if((neighbors & 0b01011010) == 0b00001000){img = images.get(10);}
			else if((neighbors & 0b01011010) == 0b01000000){img = images.get(11);}
			else if((neighbors & 0b01011010) == 0b00010000){img = images.get(12);}
			else if((neighbors & 0b11111111) == 0b11011110){img = images.get(13);}
			else if((neighbors & 0b11111111) == 0b01111011){img = images.get(14);}
			else if((neighbors & 0b01111010) == 0b01101000){img = images.get(15);}
			else if((neighbors & 0b11111010) == 0b11111000){img = images.get(16);}
			else if((neighbors & 0b11011010) == 0b11010000){img = images.get(17);}
			else if((neighbors & 0b01011010) == 0b00000000){img = images.get(18);}
			else if((neighbors & 0b01111011) == 0b01101010){img = images.get(19);}
			else if((neighbors & 0b11011110) == 0b11010010){img = images.get(20);}
			else if((neighbors & 0b11111111) == 0b11011111){img = images.get(21);}
			else if((neighbors & 0b11111111) == 0b01011111){img = images.get(22);}
			else if((neighbors & 0b11111111) == 0b01111111){img = images.get(23);}
			else if((neighbors & 0b01011011) == 0b00001010){img = images.get(24);}
			else if((neighbors & 0b01011010) == 0b00011000){img = images.get(25);}
			else if((neighbors & 0b01011111) == 0b00011010){img = images.get(26);}
			else if((neighbors & 0b01011110) == 0b00010010){img = images.get(27);}
			else if((neighbors & 0b01111011) == 0b01001011){img = images.get(28);}
			else if((neighbors & 0b01011110) == 0b01010110){img = images.get(29);}
			else if((neighbors & 0b01011010) == 0b01000010){img = images.get(30);}
			else if((neighbors & 0b01111011) == 0b01001010){img = images.get(31);}
			else if((neighbors & 0b11111111) == 0b01011010){img = images.get(32);}
			else if((neighbors & 0b11011110) == 0b01010010){img = images.get(33);}
			else if((neighbors & 0b01011111) == 0b00011110){img = images.get(34);}
			else if((neighbors & 0b01011111) == 0b00011011){img = images.get(35);}
			else if((neighbors & 0b01111010) == 0b01001000){img = images.get(36);}
			else if((neighbors & 0b11111010) == 0b01011000){img = images.get(37);}
			else if((neighbors & 0b11011010) == 0b01010000){img = images.get(38);}
			else if((neighbors & 0b11111010) == 0b11011000){img = images.get(39);}
			else if((neighbors & 0b11111010) == 0b01111000){img = images.get(40);}
			else{
			g.setColor(new Color(188, 108, 37));
			}
		}
		//draw field as rectangle
		g.fillRect(x,y, width, height);
		if(img != null)
			g.drawImage(img, x, y, width, height, null);
		//check if there's ghost or pacman on this field
		if(this.obj_view != null){
			this.obj_view.paintComponent(g);
		}
		if(field.getMarks() != null){

			if(maze.showMarks())
		for(var mark: field.getMarks().entrySet()){
			if(mark.getValue() != null){
				g.setColor(mark.getValue());
				g.fillRect(x,y, width, height);
				g.setColor(Color.BLACK);
				g.drawString(String.format("%d", mark.getKey()), x + mark.getKey() * 7,y);
			}
		}
		}

		//if (this.field instanceof WallField) {
		//	g.setColor(Color.RED);
		//	g.setFont(new Font("Arial", Font.PLAIN, 20));
		//	g.drawString(String.format("%d", neighbors), x, y+ 20);
		//}
		
	}

	public final void update(Observable field) {
		CommonMaze maze = this.field.get_maze();
		int num_rows = maze.numRows();
		int num_cols = maze.numCols();

		int width = Math.round(this.presenter.get_width() / num_cols);
		int height = Math.round(this.presenter.get_height() / num_rows);
		
		int x = Math.round(this.field.get_y() * width);
    	int y = Math.round(this.field.get_x() * height);
		
		if(field instanceof CommonField && !(this.field).isEmpty()){
			CommonMazeObject obj = (this.field).get();
	
			if(obj instanceof KeyObject){
				this.obj_view = new KeyView(this, obj);
				this.obj_view.setX(x);
				this.obj_view.setY(y);
				this.obj_view.setWidth(width);
				this.obj_view.setHeight(height);
			}
			else if (obj instanceof GateObject){
				this.obj_view = new GateView(this, obj);
				this.obj_view.setX(x);
				this.obj_view.setY(y);
				this.obj_view.setWidth(width);
				this.obj_view.setHeight(height);
			}
				
			
		}else{
			this.obj_view = null;
		}
		this.repaint();
	}
}
