/**
 * POPIS
 *
 * @author xvrbam03
 * @author xbrazi01
 */
package IJA.App.tool.view;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import IJA.App.game.PacmanObject;
import java.util.List;

/**
 * PacmanView class is responsible for rendering a Pacman object in the maze.
 * It implements the ComponentView interface.
 */
public class PacmanView implements ComponentView{
	PacmanObject object = null;

	/**
     * Constructor for the PacmanView class.
     * @param m the PacmanObject this view will represent
     */
	public PacmanView(PacmanObject m){
		this.object = m;
	}
	
	public void paintComponent(Graphics g){
		int width = this.object.getObjectWidth();
		int height = this.object.getObjectHeight();
		// color for body

		List<Image> images = object.getImage();

		Image image = images.get(this.object.dirImage);

		if (image != null){
			g.drawImage(image, object.get_x(), object.get_y(), width - 4, height - 4, null);
		}
		else {
			g.setColor(new Color(255, 241, 10));
			g.fillOval(object.get_x(), object.get_y(), width - 4, height - 4);
			// draw pacman's mouth
			g.setColor(new Color(254, 250, 224));
			int[] xarr = {width, width, width/2} ;
			int[] yarr = {height*3/4, height/4, height/2};
			g.fillPolygon(xarr, yarr, 3);
		}
		
	}

	public void setX(int x) {
        throw new UnsupportedOperationException("Not implemented for PacmanView");
    }
    
    public void setY(int y) {
        throw new UnsupportedOperationException("Not implemented for PacmanView");
    }
    
    public void setWidth(int width) {
        throw new UnsupportedOperationException("Not implemented for PacmanView");
    }
    
    public void setHeight(int height) {
        throw new UnsupportedOperationException("Not implemented for PacmanView");
    }

}
