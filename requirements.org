#+title: Homework2 Requirements
#+LATEX_CLASS: orgticle
#+AUTHOR: Matěj Vrba <xvrbam03> Tomáš Bražina <xbrazi01>
#+OPTIONS: toc:nil

* Ukol 02

| Vytvoření rozhraní podle Dokumentace       | xvrbam03 | 23.3 |
| Přidání Objektu duca                       | xvrbam03 | 23.3 |
| Implementace MazeTester                    | xvrbam03 | 28.3 |
| Úprava kódu z 1. Úkolu pro Homework2 testy | xvrbam03 | 25.3 |
| Částečná refaktorizace FieldView           | xvrbam03 |  5.4 |
| Implementacia MazePresenter                | xbrazi01 |  4.4 |
| Implementacia AbstractObservableField      | xbrazi01 |  3.4 |
| Implementacia tool/view rozhrania          | xbrazi01 |  5.4 |

* Projekt

| Implementace Návrhového vzoru command                  | xvrbam03 | 23.4 |
| Zkontrolovat generoování programové dokumentace        | xbrazi01 |  6.5 |
| Vytvoření/najití dalších map                           | xvrbam03 | 26.4 |
| Implementace ovládání pomocí Návrh. vzoru command      | xbrazi01 | 27.4 |
| Ukládání průběhu hry do paměti                         | xvrbam03 | 28.4 |
| Ukládání průběhu hry do souboru                        | xvrbam03 | 28.4 |
| Ovládání Hráče pomocí WSAD, vyžaduje 4 (N. V. command) | xbrazi01 | 30.4 |
| Ovládání Hráče pomocí myši, vyžaduje 4 (N. V. command) | xvrbam03 | 30.4 |
| Ovládání hry z uloženého průběhu, vyžaduje 4 a 5       | xvrbam03 | 30.4 |
