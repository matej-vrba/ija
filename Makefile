##
# Project Title
#
# @file
# @version 0.1
SRC = $(shell find . | grep java)
.PHONY = build test

test: ija/ija2022/homework2/Homework2Test.class
	java -cp ./build:ijatool.jar:junit-platform-console-standalone-1.6.0.jar org.junit.runner.JUnitCore ija.ija2022.homework2.Homework2Test

ija/ija2022/homework2/Homework2Test.class: $(SRC)
	javac -cp .:ijatool.jar:junit-platform-console-standalone-1.6.0.jar -d build ija/ija2022/homework2/Homework2Test.java

viz: ija/ija2022/homework2/Homework2.class
	java -cp ./build:ijatool.jar ija.ija2022.homework2.Homework2

ija/ija2022/homework2/Homework2.class: $(SRC)
	javac -cp .:ijatool.jar -d build ija/ija2022/homework2/Homework2.java


# end
